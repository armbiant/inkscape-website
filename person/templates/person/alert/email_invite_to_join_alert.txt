{% load i18n %}
{% blocktrans with recipient=alert.user %}Dear {{ recipient }},{% endblocktrans %}
 
{% blocktrans with actor=instance.acting_user team=instance.team %}You have been invited to join team {{ team }} by '{{ actor }}'.{% endblocktrans %}

{% blocktrans %}Accept invitation{% endblocktrans %}: {{ site }}{{ instance.get_invitation_url }}
{% blocktrans %}Visit Team page{% endblocktrans %}: {{ site }}{{ instance.team.get_absolute_url }}
{% blocktrans %}Visit Invitor's page{% endblocktrans %}: {{ site }}{{ instance.acting_user.get_absolute_url }}
