#
# Copyright 2016, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Stats app settings
"""

import re
from django.conf import settings
from .countries import LANGUAGES

ROOT = IOError("Please define a LOGS_ROOT where your logs can be found.")
LANGUAGE_CODES = list(LANGUAGES)

UNITS = {
  'size': 'bytes',
  'delay': 'ms',
}

SEARCHES = [
    re.compile('https?://(www\.|search\.)?(?P<site>[^\.]+).*[\?&]q=(?P<q>[^&]+).*'),
    re.compile('https?://(www\.|search\.)?(?P<site>[^\.]+).*\/search\?(.*)p=(?P<q>[^&]+).*'),
    re.compile('https?://(www\.|search\.)?(?P<site>(google|yahoo|bing|yandex|qwant|duckduckgo))'),
]

IGNORE = ['rest', 'method']

PATH_FIELDS = ['size', 'delay', 'refer', 'link', 'status', 'search']

LOGS = {
    # Nginx tells us the os and browser, language and most info.
    'nginx': {
      'rex': re.compile(
        r'^(?P<ip>\d+\.\d+\.\d+\.\d+)'
        r' - - '
        r'\[(?P<D>\d+)\/(?P<M>[a-zA-Z]+)\/(?P<Y>\d+):'
        r'(?P<h>\d+):(?P<m>\d+):(?P<s>\d+) (?P<tz>[+\-]\d+)\] '
        r'\"(?P<method>[A-Z]+) '
        r'(https://inkscape.org)?(?P<path>\/[^\s]*) '
        r'HTTP\/[\d\.]+\" '
        r'(?P<status>\d+) '
        r'(?P<size>\d+) '
        r'\"(?P<refer>[^\"]*)\" '
        r'\"(?P<agent>[^\"]+)\"'
        r'(?P<rest>.*)$'),
      'ignore': ('rest', 'other', 'tz'),
    },
    # Wsgi tells us how long requests took, we could take other information
    'wsgi': {
      'rex': re.compile(
        r'^\[pid\: (?P<pid>\d+)'
        r'\|app\: (?P<app>\d+)'
        r'\|req\: \d+\/\d+\] '
        r'(?P<ip>[\d\.]+) \(\) '
        r'\{\d+ vars in \d+ bytes\} '
        r'\[\w\w\w (?P<M>\w\w\w) (?P<D>\d+) '
        r'(?P<h>\d+):(?P<m>\d+):(?P<s>\d+) '
        r'(?P<Y>\d+)\] '
        r'(?P<method>\w+) '
        r'(?P<path>[^\s]+) => '
        r'generated (?P<size>\d+) bytes '
        r'in (?P<delay>\d+) msecs '
        r'\(HTTP\/1.[10] (?P<status>\w+)\) '
        r'\d+ headers in \d+ bytes '
        r'\(\d+ switches on core \d+\)'
        r'(?P<rest>.*)$'),
      'ignore': ('app', 'pid', 'country', 'size', 'method', 'status', 'count')
    },
}

rexer = lambda rec: re.compile(rec, flags=re.IGNORECASE)

FILTERS = {
  'browser': [
    (rexer(r'(.*)__(\d+)\.0+$'), r'\1__\2', True),
    # Ignore developer builds
    (rexer(r'(.*)(nightly|alpha|beta|frame)(.*)__(.*)'), r'', False),
    (rexer(r'.*(bot|spider|scraper|crawler|crawling|archiv|scrapy|sleuth|simplepie|bing|winhttp|apache|slurp).*__(.*)'), r'Bot__\1', False),
    (rexer(r'(.*(ning|facebook|pinterest|wordpress|twitter|whatsapp).*)__(.*)'), r'Social Feed__\1', False),
    (rexer(r'(.*(mail|outlook|thunderbird).*)__(.*)'), r'Mail__\1', False),
    (rexer(r'(.*)(mobile|mini|tablet|ios|iphone|samsung)(.*)__(.*)'), r'Mobile__\1\2\3M', False),
    (rexer(r'(.*)(edge|zune|sogou)(.*)__(.*)'), r'IE__\4', False),
    (rexer(r'(.*)(google|yandex|safari|chromium|uc browser|android|vivaldi|vienna|chrome *webview)(.*)__(.*)'), r'Chrome__\4', False),
    (rexer(r'(.*)(netscape|waterfox|otter|iceweasel)(.*)__(.*)'), r'Firefox__\4', False),
    (rexer(r'(.*)(rss|feedbin|reader|netnewswire|seamonkey)(.*)__(.*)'), r'RSS__\2', False),
    (rexer(r'(.*)(curl|wget|bash|http|phantomjs|python|java)(.*)__(.*)'), r'Script__\2', False),
  ],
  'os': [
    (rexer(r'(.*)__$'), r'\1__Unknown', True),
    (rexer(r'(ios)__(\d).+'), r'\1__\2', False),
    (rexer(r'(mac os x)__(([\d\.])+|unknown)'), r'macOS__\2', True),
    (rexer(r'.*(chrome os).*__(.*)'), r'Linux__\1', False),
    (rexer(r'(.*)(symbian|blackberry os|maemo|meego|tizen|kaios|kindle)(.*)__(.*)'), r'Other Phone__\2', False),
    (rexer(r'(.*)(openbsd|freebsd|netbsd|solaris)(.*)__(.*)'), r'Unix__\2', False),
    (rexer(r'(.*(ubuntu|fedora|suse|debian|red hat|mandriva|gentoo|mageia|centos|linux mint|arch linux).*)__(.*)'), r'Linux__\1', False),
    (rexer(r'linux__linux'), r'linux__unknown', False),
  ],
  'device': [
    (rexer(r'^.*spider.*$'), r'', False),
    (rexer(r'^.*unversioned.*$'), r'', False),
    (rexer(r'^(.*)__\1 \1 (.*)'), r'\1__\2', False),
    (rexer(r'^(.*)__\1 (.*)'), r'\1__\2', False),
    (rexer(r'(.*)__(nexus .*)'), r'Asus__\2', False),
    (rexer(r'(sonyericsson)__(.*)'), r'Sony__SonyEricsson \2', False),
    (rexer(r'generic_android__.*tablet.*'), r'Generic__Tablet', False),
    (rexer(r'generic_android__(.*)'), r'Generic__Android', False),
  ],
}

# Anything not in this list is ignored
ALLOWED_OS = [
    'Linux', 'Windows', 'macOS', 'iOS', 'Android', 'Unix', 'Other Phone',
]
ALLOWED_BROWSER = [
    'Firefox', 'IE', 'Safari', 'Opera', 'Chrome',
    'Bot', 'Script', 'RSS', 'Social Feed', 'Mail', 'Mobile', 'Other',
]

# Used to test settings (do not override)
TEST_ERROR = IOError("Caused an error")

def get_setting(key, value=KeyError, prefix='LOGS'):
    """
      Attempt to get site settings, fall back to app setting,
      raise an exception if the value is requird.

      The default prefix is 'LOGS'
    """
    value = getattr(settings, prefix + '_' + key,
               globals().get(key, value))
    if isinstance(value, Exception):
        raise value
    elif value is KeyError:
        raise value("Setting not found: %s" % key)
    compiler = 'compile_' + key.lower()
    if compiler in globals():
        return globals()[compiler](value)
    return value
